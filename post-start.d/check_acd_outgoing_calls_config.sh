#!/bin/bash

# Check ACD outgoing calls config
queueskills_conf="/etc/asterisk/queueskills.conf"
gen_agent_script="/usr/local/sbin/generate_agent_skills.py"
xuc_outcall_acd_subroutine="/etc/asterisk/extensions_extra.d/xuc_outcall_acd.conf"

if [ -f "$gen_agent_script" ] \
|| [ -f "$xuc_outcall_acd_subroutine" ] \
|| (grep -oP -m 1 "$gen_agent_script" "$queueskills_conf" &> /dev/null); then
	cat <<-EOF
	*****************************************************************************************
	* WARNING: ACD outgoing calls configuration was detected				*
	* 											*
	* Configuration files and dialplan subroutine for ACD outgoing calls for call blending	*
	* was integrated to XiVO. The old configuration MUST be manually removed.		*
	* Please follow the removal procedure described in XiVO Aldebaran documentation.	*
	* See Release Notes > Aldebaran > Upgrade Polaris to Aldebaran.				*
	*****************************************************************************************
	EOF
fi

