#!/bin/bash

# Check if old recording package are installed

list_packages_with_filter() {
    filter="$1"
    aptitude -q -F '%p' --disable-columns search "$filter"
}

display_xivo_recording_notice() {
	cat <<-EOF
	************************************************************************************************
	*                                      WARNING - Recording                                     *
	* Packages xivo-recording and call-recording-filtering were removed.                           *
	*                                                                                              *
	* You MUST follow this specific upgrade procedure in documentation :                           *
	* Installation & Upgrade > XiVO Installation & Upgrade > Upgrade > Specific procedures >       *
	*     XiVOCC Recording upgrade procedure                                                       *
	*                                                                                              *
	************************************************************************************************
	EOF
}


is_xivo_recording_installed=$(list_packages_with_filter "?or(?installed?name(\"^xivo-recording$\"),?config-files?name(\"^xivo-recording$\"))")
is_call_filtering_installed=$(list_packages_with_filter "?or(?installed?name(\"^call-recording-filtering$\"),?config-files?name(\"^call-recording-filtering$\"))")

if ! [ -z "$is_xivo_recording_installed" ] || ! [ -z "$is_call_filtering_installed" ]; then
    apt-get remove --yes xivo-recording call-recording-filtering
    display_xivo_recording_notice
fi
