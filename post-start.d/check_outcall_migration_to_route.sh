#!/bin/sh

set -e

# Outgoing call were replaced by route in 2019.05.00
if [ "$XIVO_VERSION_INSTALLED" \> "2019.05.00" ]; then
	exit 0
fi

# zgrep command with wildcard can't be used for test because exit status corresponds to the last tested file
if grep -s '\[MIGRATE_OUTCALL\].*WARNING' /var/log/postgresql/postgresql-11-main.log > /dev/null \
    || grep -s '\[MIGRATE_OUTCALL\].*WARNING' /var/log/postgresql/postgresql-11-main.log.1 > /dev/null; then
	cat <<-EOF
	
	******************************************************************************************
	*  WARNING: Outgoing calls were migrated to Routes and there were some problems          *
	*                                                                                        *
	*   You MUST check the upgrade log with the following command:                           *
	*   # zgrep '\[MIGRATE_OUTCALL\].*WARNING' /var/log/postgresql/postgresql-11-main.log*   *
	*                                                                                        *
	*   This will give you the outcall names that had problems.                              *
	*   Please follow the "Upgrade Boréalis to Callisto > After Upgrade" section             *
	*   of XiVO Callisto documentation.                                                      *
	******************************************************************************************
	
	EOF
fi
