#!/bin/sh

set -e

# Outgoing call was removed in 2018.03
if [ "$XIVO_VERSION_INSTALLED" \> "2018.03.00" ]; then
	exit 0
fi

if grep MIGRATE_OUTCALL_FWD /var/log/xivo-upgrade.log > /dev/null \
    || grep -s MIGRATE_OUTCALL_FWD /var/log/xivo-upgrade.log.1 > /dev/null; then
	cat <<-EOF
	
	*********************************************************************************
	*   WARNING: Outgoing call was removed from redirections.                       *
	*                                                                               *
	*   You MUST check the xivo-upgrade log with the following command:             *
	*   # zgrep MIGRATE_OUTCALL_FWD /var/log/xivo-upgrade.log*                      *
	*                                                                               *
	*   Then you MUST edit each object to reconfigure the changed destination.      *
	*   Please follow the "Upgrade Polaris to Aldebaran > After Upgrade" section    *
	*   of XiVO Aldebaran documentation.                                            *
	*********************************************************************************
	
	EOF
fi
