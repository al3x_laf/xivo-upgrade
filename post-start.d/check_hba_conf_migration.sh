#!/bin/sh

if [ "$XIVO_VERSION_INSTALLED" \> "2019.05.07" ]; then
	exit 0
fi

zgrep '\[MIGRATE_HBA_CONF\]' /var/log/postgresql/postgresql-11-main.log* 2> /dev/null

# zgrep exit status corresponds to the last tested file
exit 0
